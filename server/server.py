import json
import math
import requests
from flask import Flask, request, abort
from flask_socketio import SocketIO, emit
from flask_cors import CORS
from CalculationRequest import *
from MapCreator import *



app = Flask(__name__)
CORS(app)
socketio = SocketIO(app, cors_allowed_origins="*")

@app.route("/")
def helloWorld():
  return "Hello : /create-map [POST] {maxSize >= 10 & <= 10000, pointNumber >= 2 & <= 10% maxSize², maxLinksNumber > 1 & <= pointNumber}"

@app.route('/create-map', methods=['POST'])
def createmap():
    mapCreator = MapCreator(request.json['maxSize'], request.json['pointNumber'], request.json['maxLinksNumber'])
    if mapCreator.maxSize < 10 or mapCreator.maxSize > 10000 or mapCreator.pointNumber < 2 or  mapCreator.pointNumber > math.pow(mapCreator.maxSize,2) /10 or mapCreator.maxLinksNumber >= mapCreator.pointNumber or mapCreator.maxLinksNumber <= 1:
        abort(400) 
    
    mapCreated = mapCreator.make()
    return json.dumps(mapCreated)

@socketio.on('ask-path')
def handleAskPath(datasObject):
    print('Rcv request')
    if 'start' not in datasObject or 'finish' not in datasObject or 'graph' not in datasObject:
      emit('bad-request', "Bad arguments rcv {graph: {}, start: X, finish: Y}") 
    requestClient = CalculationRequest(datasObject, socketio, request.sid)
    requestClient.start()
    print("Emit start-calculating")
    emit('start-calculating', 'Your calculation from '+datasObject["start"]+" to "+datasObject["finish"]+" has start!") 

if __name__ == '__main__':
     socketio.run(app, host='0.0.0.0')#, host='0.0.0.0')#, debug=True)
