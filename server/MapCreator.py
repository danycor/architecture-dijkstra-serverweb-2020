from random import *
import random
import math
import time
import json

class InternalMakingFailException(Exception):
    pass

class MapCreator:
    SPEED_UP_RATIO = 0.1
    SPEED_DOWN_RATIO = 0.2
    MAX_SPEED_UP = 4.0
    MIN_SPEED_DOWN = 0.2

    def __init__(self, maxSize, pointNumber, maxLinksNumber):
        self.maxSize = maxSize
        self.pointNumber = pointNumber
        self.maxLinksNumber = maxLinksNumber
        self.objectMap = {}

    def createNodes(self):
        with open('name.json', 'r', encoding='utf-8') as cityNamesJson:
            cityNames = json.loads(cityNamesJson.read())
            self.objectMap = {}
            for i in range(0, self.pointNumber):
                cityId = randint(0,100)
                self.objectMap[cityNames[randint(0,36000)]+"-"+str(cityId)] = {}
    
    def createPositions(self, node):
        havePosition = False
        while havePosition != True:
            x = randrange(0, self.maxSize-1)
            y = randrange(0, self.maxSize-1)
            canUsePosition = True
            # Check already existing node position
            for allNode in self.objectMap:
                if 'position' in self.objectMap[allNode].keys():
                    if self.objectMap[allNode]['position']['x']  == x and self.objectMap[allNode]['position']['y']  == y:
                        canUsePosition = False
            if canUsePosition:
                havePosition = True
                self.objectMap[node]['position'] = {}
                self.objectMap[node]['position']['x'] = x
                self.objectMap[node]['position']['y'] = y

    def createLinks(self, node):
        if 'links' not in self.objectMap[node].keys():
            self.objectMap[node]['links'] = {}
        nbHaving = self.objectMap[node]['links'].__len__()
        nbToHave = randrange(1, self.maxLinksNumber)
        while nbHaving < nbToHave:
            nodeToLink = self.getValidNodeToLink(node)
            if nodeToLink != False:
                if 'links' not in self.objectMap[nodeToLink].keys():
                    self.objectMap[nodeToLink]['links'] = {}
                if self.objectMap[nodeToLink]['links'].keys().__len__() < self.maxLinksNumber:
                    nbHaving += 1
                    self.setUpLink(node, nodeToLink)
            else:
                break

    def getValidNodeToLink(self, node):
        nodeKeys = {}
        for nodeKey in self.objectMap.keys():
            nodeKeys[nodeKey] = self.getDistanceBetween(node, nodeKey)
        nodeKeys = {k: v for k, v in sorted(nodeKeys.items(), key=lambda item: item[1])}
        for nodekey in nodeKeys:
            if nodekey != node:
                if nodekey not in self.objectMap[node]['links'].keys():
                    if node not in self.objectMap[nodekey]['links'].keys():
                        if self.objectMap[nodekey]['links'].keys().__len__() < self.maxLinksNumber:
                            return nodekey
        return False
    def setUpLink(self, nodeA, nodeB):
        length = int(self.getDistanceBetween(nodeA, nodeB)* 1000)
        speed = 1
        if randint(0,100) < self.SPEED_DOWN_RATIO * 100:
            speed = random.uniform(self.MIN_SPEED_DOWN, 1.0)
        elif randint(0,100) < self.SPEED_UP_RATIO * 100:
            speed = random.uniform(1.0, self.MAX_SPEED_UP)
        speed = round(speed, 2)
        self.objectMap[nodeA]['links'][nodeB] = {}
        self.objectMap[nodeA]['links'][nodeB]['length'] = length
        self.objectMap[nodeA]['links'][nodeB]['speed'] = speed
        self.objectMap[nodeB]['links'][nodeA] = {}
        self.objectMap[nodeB]['links'][nodeA]['length'] = length
        self.objectMap[nodeB]['links'][nodeA]['speed'] = speed

    def getDistanceBetween(self, nodeA, nodeB):
        return math.sqrt(math.pow(self.objectMap[nodeA]['position']['x']-self.objectMap[nodeB]['position']['x'], 2)+math.pow(self.objectMap[nodeA]['position']['y']-self.objectMap[nodeB]['position']['y'], 2))

    # Make the map
    def make(self):
        # Create objects / Nodes
        self.createNodes()
        # Fill nodes by position
        for node in self.objectMap:
            self.objectMap[node]['links'] = {}
            self.createPositions(node)
        # Fill nodes by links
        for node in self.objectMap:
            self.createLinks(node)
        return self.objectMap

