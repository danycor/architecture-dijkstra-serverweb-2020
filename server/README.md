# Architecture-Dijkstra-ServerWeb-2020

## Commandes

* `pip install flask`
* `pip install flask-socketio`
* `pip install ipdb`
* `pip install requests`
* `pip install flask-cors`

* `import pdb; pdb.set_trace()`


## API
|Route|Methode|Parametres|Retour|
|-----|-------|----------|------|
|`/`|GET|-|The creation map route informations|
|`/create-map`|POST|`{"maxSize": [10-10000], "pointNumber": [2-10% maxsize²], "maxLinksNumber": [1-pointNumber[}`|The map `{nodeName: {"links":{}, "position": {}}, nodeName2: {"links":{}, "position": {}}}`|

## Web socket

|Evenement|Direction|Parametres|
|---|---|---|
|`ask-path`|Client -> Server|`{"graph": {}, "start": "", "finish": ""}`|
|`start-calculating`|Server -> Client (après un `ask-path`)|String|
|`calculation-end`|Server -> Client (à la fin du calcul)|`{"result": {"list": [], "length": 0}, "steps": [[],[],[],...}`|
|`bad-request`|Server -> Client|Error message. Bad arguments|