run:
	docker-compose down
	docker-compose up -d
update:
	docker-compose down
	git pull
	docker-compose exec python-server "pip install --no-cache-dir -r requirements.txt"
	docker-compose up -d