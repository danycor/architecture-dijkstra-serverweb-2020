from threading import Thread
import json
import time
from flask_socketio import SocketIO, emit
from dijkstraalgorythm.dijkstra_oop import *
from dijkstraalgorythm.loader import *

class CalculationRequest(Thread):
    def __init__(self, dataToAlgo, socketio, client):
        Thread.__init__(self)
        self.datas = {}
        self.datas["graph"] = self.convertMapToAlgoMap(dataToAlgo["graph"])
        self.datas["start"] = dataToAlgo["start"]
        self.datas["finish"] = dataToAlgo["finish"]
        self.client = client
        self.socketio = socketio
    
    def convertMapToAlgoMap(self, mapObject):
        for node in mapObject:
            for link in mapObject[node]['links']:
                mapObject[node][link] = int(float(mapObject[node]['links'][link]["length"])*10000.0/mapObject[node]['links'][link]["speed"])
            del mapObject[node]['links']
            del mapObject[node]['position']
        return mapObject

    def run(self):
        graph = generate_graph(self.datas["graph"])
        datas = dijkstra_oop(graph, self.datas["start"], self.datas["finish"])
        true_data = {
            'result': {"list": datas[0], "length": datas[1]},
            'steps': []
        }
        print('Emit calculation end')
        self.socketio.emit('calculation-end', true_data, room=self.client)